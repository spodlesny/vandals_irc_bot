#!/usr/bin/python
# -*- coding: utf8 -*-

import config, core, time, urllib, urllib2, commands, re, os


def ping(data, irc): 
  if data.find('!PING') != -1 or data.find('!ping') != -1:
    sprava = "PONG"
    irc.send('PRIVMSG #'+config.chan+' :'+sprava+'\r\n')
