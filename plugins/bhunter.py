#!/usr/bin/python
# -*- coding: utf8 -*

import json
import urllib2
import config, core
import thread
import random

def aktualne_bitky():
	response = urllib2.urlopen("https://www.cscpro.org/secura/battles.json")
	json_object = response.read()
	response_dict = json.loads(json_object)
	bitky = []
	for x in range(len(response_dict["battles"])):
		if response_dict["battles"][x]["region"] != "Practice battle" and response_dict["battles"][x]["region"] != "Civil war":
			bitky.append(response_dict["battles"][x]["id"])
	print type(bitky[1])
	return bitky

def cas_do_konca(id_bitky, irc):
	try:
		response = urllib2.urlopen("https://www.cscpro.org/secura/battle/"+str(aktualne_bitky()[aktualne_bitky().index(int(config.id_bitky))])+".json")
		json_object = response.read()
		response_dict = json.loads(json_object)
		cas = response_dict["time"]
		print str(response_dict["time"]["hour"])+":"+str(response_dict["time"]["minute"])+":"+str(response_dict["time"]["second"])

		if response_dict["time"]["hour"] == 0:
			if response_dict["time"]["minute"] == 5:
				print "t5"			
				return "t5"
			elif response_dict["time"]["minute"] == 4:
				print "t4"
				return "t4"
			elif response_dict["time"]["minute"] == 3:
				print "t3"
				return "t3"
			elif response_dict["time"]["minute"] == 2:
				print "t2"
				return "t2"
			elif response_dict["time"]["minute"] == 1:
				print "t1"
				return "t1"
			else:
				print response_dict["time"]["minute"]
				print type(response_dict["time"]["minute"])
				return None

		else:
			print str(response_dict["time"]["hour"])+":"+str(response_dict["time"]["minute"])+":"+str(response_dict["time"]["second"])
			return None
	except ValueError as err:
		if config.vypis_chybu == True:
			irc.send('PRIVMSG #'+config.chan+' :'+"Bitka nebola nájdená, alebo sa jedná o civil/practise battle"+'\r\n')
			config.vypis_chybu = False
			print err

def bhunter(data, irc):
	if data.lower().find('order:') != -1:
		neparsovany_vstup = data.lower().split()
		if len(str(neparsovany_vstup[-1])) != 5:
			irc.send('PRIVMSG #'+config.chan+' :'+'nespravne zadana hodnota. Zadaj hodnotu vo formate: "order: xxxxx"'+'\r\n')
		else:
			config.id_bitky = neparsovany_vstup[-1]
			print config.bhunter_start
			irc.send('PRIVMSG #'+config.chan+' :'+'Bot sleduje bitku: http://secura.e-sim.org/battle.html?id='+str(config.id_bitky)+'\r\n')

	elif data.lower().find('start bot') != -1:
		irc.send('PRIVMSG #'+config.chan+' :'+'Funkcia nie je prevádzky schopná a jej spustenie môze spôsobit pad celeho bota'+'\r\n')
		irc.send('PRIVMSG #'+config.chan+' :'+'Funkcia kontrola casu do konca je upsesne aktivovana. Pre nastavenie aktualnej bitky, zadaj prikaz: "order: xxxxx"'+'\r\n')
		config.bhunter_start = True
		print config.bhunter_start
	else:
		print config.bhunter_start
		if config.bhunter_start == True:
				print "Fungujem"
				print config.id_bitky
				if cas_do_konca(config.id_bitky, irc) is not None:
					irc.send('PRIVMSG #'+config.chan+' :'+str(cas_do_konca(config.id_bitky, irc))+'\r\n')
					print "Vypisujem"
				else:
					print "Nevypisujem lebo: "+str(cas_do_konca(config.id_bitky, irc))
