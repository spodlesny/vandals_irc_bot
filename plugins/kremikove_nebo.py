#!/usr/bin/python
# -*- coding: utf8 -*-

import config, core, time, urllib, urllib2, commands, re, os

def kremikove_nebo(data, irc):
    if data.lower().find('!kremikove_nebo') != -1:
        sprava = "ok"
        irc.send('PRIVMSG #'+config.chan+' :'+sprava+'\r\n')
        irc.send('QUIT Vidim svetlo na konci optiky\r\n')
        exit()