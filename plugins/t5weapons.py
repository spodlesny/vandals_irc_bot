#!/usr/bin/python
# -*- coding: utf8 -*
# author: symon6

import json
import urllib2
import datetime
import time
from datetime import datetime, timedelta
import random
import config, core


def get_player_id(player_name, irc):
	while True:
		try:
			response = urllib2.urlopen("http://secura.e-sim.org/apiCitizenByName.html?name="+player_name.lower())
			json_object = response.read()
			response_dict = json.loads(json_object)
			player_id = response_dict["id"]
			return player_id
		except KeyError as err:
			try:
				response = urllib2.urlopen("http://secura.e-sim.org/apiCitizenById.html?id="+player_name)
				json_object = response.read()
				response_dict = json.loads(json_object)
				player_id = response_dict["id"]
				return player_id
			except KeyError:
				response = "Hrác nebol najdeny. Skontroluj syntax alebo zadaj jeho id"
				irc.send('PRIVMSG #'+config.chan+' :'+response+'\r\n')

def get_player_name(player_id):
		while True:
			try:
				response = urllib2.urlopen("http://secura.e-sim.org/apiCitizenById.html?id="+str(player_id))
				json_object = response.read()
				response_dict = json.loads(json_object)
				player_id = response_dict["login"]
				return player_id
			except Exception as err:
				response = str(err)
				print response

def get_list_of_vandals(battle_id,round_id, irc):
	zoznam = []
	try:
		response = urllib2.urlopen("http://secura.e-sim.org/apiFights.html?battleId="+str(battle_id)+"&roundId="+str(round_id))
		json_object = response.read()
		response_dict = json.loads(json_object)

		koniec_kola = datetime.strptime(response_dict[0]["time"], "%d-%m-%Y %H:%M:%S:%f")
		zaciatok_t1 = koniec_kola - timedelta(minutes=1, seconds=10)

		for x in range(len(response_dict)):
			try:
				if response_dict[x]["militaryUnit"] == 271: #zabudol som na ludi co nemaju MU!
					cas_utoku = datetime.strptime(response_dict[x]["time"], "%d-%m-%Y %H:%M:%S:%f")
					if zaciatok_t1 <= cas_utoku <= koniec_kola:
						try:
							zoznam.index(response_dict[x]["citizenId"])
						except ValueError:
							zoznam.append(response_dict[x]["citizenId"])
			except KeyError: #ignorovanie ludi co nemaju MU
				pass
		return zoznam


	except KeyError as err:
		response = "ID bitky alebo cislo bitky je nespravne"
		irc.send('PRIVMSG #'+config.chan+' :'+response+'\r\n')


def get_number_of_weapons(player_id,battle_id, round_id, irc):
	while True:
		try:
			battle_id = str(battle_id)
			round_id = str(round_id)

			response = urllib2.urlopen("http://secura.e-sim.org/apiFights.html?battleId="+battle_id+"&roundId="+round_id)
			json_object = response.read()
			response_dict = json.loads(json_object)
			
			pocet_zbrani_t1 = 0
			pocet_zbrani_t2 = 0
			pocet_zbrani_t3 = 0
			pocet_zbrani_t4 = 0
			pocet_zbrani_t5 = 0

			koniec_kola = datetime.strptime(response_dict[0]["time"], "%d-%m-%Y %H:%M:%S:%f")


			zaciatok_t1 = koniec_kola - timedelta(minutes=1, seconds=10) #minuta 10s je z dôvodu ze stranka sa môze nacitat dlhsie a casovac sa moze oneskorit

			for x in range(len(response_dict)):
				cas_utoku = datetime.strptime(response_dict[x]["time"], "%d-%m-%Y %H:%M:%S:%f")
				if zaciatok_t1 <= cas_utoku <= koniec_kola:
					if response_dict[x]["citizenId"] == player_id and response_dict[x]["weapon"] == 1:
						if response_dict[x]["berserk"] == True:
							pocet_zbrani_t1+=5
						if response_dict[x]["berserk"] == False:
							pocet_zbrani_t1+=1
					if response_dict[x]["citizenId"] == player_id and response_dict[x]["weapon"] == 2:
						if response_dict[x]["berserk"] == True:
							pocet_zbrani_t2+=5
						if response_dict[x]["berserk"] == False:
							pocet_zbrani_t2+=1
					if response_dict[x]["citizenId"] == player_id and response_dict[x]["weapon"] == 3:
						if response_dict[x]["berserk"] == True:
							pocet_zbrani_t3+=5
						if response_dict[x]["berserk"] == False:
							pocet_zbrani_t3+=1
					if response_dict[x]["citizenId"] == player_id and response_dict[x]["weapon"] == 4:
						if response_dict[x]["berserk"] == True:
							pocet_zbrani_t4+=5
						if response_dict[x]["berserk"] == False:
							pocet_zbrani_t4+=1
					if response_dict[x]["citizenId"] == player_id and response_dict[x]["weapon"] == 5:
						if response_dict[x]["berserk"] == True:
							pocet_zbrani_t5+=5
						if response_dict[x]["berserk"] == False:
							pocet_zbrani_t5+=1

			response = "Hráč {} minul: ".format(get_player_name(player_id))
			if pocet_zbrani_t1 != 0:
				response = response+"{} Q1 zbraní".format(pocet_zbrani_t1)
			if pocet_zbrani_t2 != 0:
							response = response+"{} Q2 zbraní".format(pocet_zbrani_t2)
			if pocet_zbrani_t3 != 0:
							response = response+"{} Q3 zbraní".format(pocet_zbrani_t3)
			if pocet_zbrani_t4 != 0:
							response = response+"{} Q4 zbraní".format(pocet_zbrani_t4)
			if pocet_zbrani_t5 != 0:
							response = response+"{} Q5 zbraní".format(pocet_zbrani_t5)

			#"Hráč {} minul: {} Q1 zbraní, {} Q2 zbraní, {} Q3 zbraní, {} Q4 zbraní, {} Q5 zbraní".format(get_player_name(player_id),pocet_zbrani_t1,pocet_zbrani_t2,pocet_zbrani_t3,pocet_zbrani_t4,pocet_zbrani_t5)

			irc.send('PRIVMSG #'+config.chan+' :'+response+'\r\n')
			return pocet_zbrani_t1
		except KeyError as err:
			response = "ID bitky alebo cislo bitky je nesprávne"
			irc.send('PRIVMSG #'+config.chan+' :'+response+'\r\n')
		except Exception as err:
			response = "Nastala neocakavana chyba: "+str(err)
			irc.send('PRIVMSG #'+config.chan+' :'+response+'\r\n')

def get_last_round(battle_id, irc):
	battle_id = str(battle_id)
	response = urllib2.urlopen("https://www.cscpro.org/secura/battle/"+battle_id+"-/"+".json")
	json_object = response.read()
	response_dict = json.loads(json_object)	
	if response_dict["status"] == "Active":
		time_of_sleep = (response_dict["time"]["hour"]*60*60)+(response_dict["time"]["minute"]*60)+response_dict["time"]["second"]
		print time_of_sleep
		time.sleep(time_of_sleep)
		return response_dict["round"]
	else:
		irc.send('PRIVMSG #'+config.chan+' :'+"Bitka sa skoncila. Nemam teda co ukazovat a cakam na dalsi order. Mas vsak na to len 277 hodin a potom spadnem nakolko toto este nie je osetrene :D "+'\r\n')
		time.sleep(1000000)			


def main(data, irc):
	#try:
		battle_id = None
		if data.lower().find('!battle') != -1: #format prikazu: !battle -t10 19410 -se
			print "working"
			while True:
				try:
					battle_id = data.split()[-2]
					round_id = get_last_round(battle_id, irc)
					vandals = get_list_of_vandals(battle_id,round_id,irc)			

					for x in range(len(vandals)):
						get_number_of_weapons(vandals[x],battle_id,round_id, irc)
				except Exception as err:
					irc.send('PRIVMSG #'+config.chan+' :'+'Niekde nastala chyba. Skontroluj ci format zapisu je: "!battle -t10 19410 -se" a ak stale nastava chyba tak kontaktuj hraca symon6 popripade ma zabi prikazom "!kremikove_nebo"'+'\r\n')
					break

	#except Exception as err:
	#	irc.send('PRIVMSG #'+config.chan+' :'+str(err)+'\r\n')
